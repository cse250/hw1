#ifndef UNTITLED_GAMELOGIC_H
#define UNTITLED_GAMELOGIC_H

#include <iostream>

class GameLogic {

private:
    str konami;
    size_t found;

public:
    bool containsKonamiCode(std::string inputString);
    virtual int computeScore(std::string inputString) =0;
};


#endif //UNTITLED_GAMELOGIC_H
