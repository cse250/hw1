#ifndef UNTITLED_TREASUREHUNTER_H
#define UNTITLED_TREASUREHUNTER_H

#include "GameBoard.h"


class TreasureHunter : public GameLogic  {
    private::

          int score;
          int differenceXL;
          int differenceXR;
          int differenceYL;
          int differenceYR
          Location treasureHunterLocation;

    public:

        //The Params are the starting locations of the Hunter, x and y respectively
        TreasureHunter(int, int, GameBoard&);
        ~TreasureHunter();

        void changeBoard(GameBoard&);
        void changeBoard(GameBoard*);
        void moveUp();
        void moveDown();
        void moveLeft();
        void moveRight();

        int differenceXleft(int current, int* previousL);
        int differenceXRight(int current, int* previousR);
        int differenceYUp(int current, int* previousU);
        int differenceYDown(int current, int* previousD);
        int foundTreasure();
        int winCondition();
        int loseCondition();

        virtual int computeScore(std::string inputString);
};

#endif //UNTITLED_TREASUREHUNTER_H
